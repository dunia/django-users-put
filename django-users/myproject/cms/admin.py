from django.contrib import admin
from .models import Contenido, Comentario
admin.site.register(Contenido)
admin.site.register(Comentario) #<--- lo registramoas
# Register your models here.
#gacemos esto porque si tenemos varios modelos en models podemos hacer que el interfaz de administracion sea solo para un subconjunto de ellos
#si entramos al /admin ahora tenemos en cms un espacio de contenidos donde aparece lo que teniamos en models clave y valor
