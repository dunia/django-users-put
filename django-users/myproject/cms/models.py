from django.db import models

# Create your models here.
#aqui van las clases
class Contenido(models.Model):

    clave = models.CharField(max_length=64) #campo de texto limitado en tamaño
    valor = models.TextField()
    #comentario = models.TextField() #contenidos con N comentario con autor y fecha pues esto no seria muy practico
    #necesitamos otra base de datos una nueva tabla cms
    def __str__(self):
        return str(self.id) + ": " + self.clave + "---" + self.valor
    #esto es para que al añadir el comentario no solo nos salga el identificador sino el contenido con su valor

    #se pueden añadir funcione s comprobar en la shell
class Comentario(models.Model):
    contenido = models.ForeignKey(Contenido, on_delete=models.CASCADE)
    #on_delete=models.CASCADE( sireve para sis e borra un contnido se borren en cascada sus comentarios
    titulo = models.CharField(max_length=64)
    cuerpo = models.TextField()
    fecha = models.DateTimeField()
    #ternmso que ligar el comentario a que conteido es
    #definir realxion contenido <-->Comentario, un contenido puede tener N ocmentarios
    #cada comentariio definir el contenido del que procede---> ForeignJey


    #despues de tod tenemos que actualizar la base de datos haciendo migrate
    #fichero dbsqlite3 , si cambiamos modelo cambiamos la base de datos
    #ejecutar python3 manage.py makemigrations lo revisamos el fichero y hacemos lo mismo pero con migrate

    def __str__(self):
        return str(self.id) + ": " + self.titulo
    #para ver emjeor el formato de los comenatarios


