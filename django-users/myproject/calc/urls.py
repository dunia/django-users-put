from django.urls import path
from . import views

urlpatterns = [
    #path('', views.index, name='index'),
    path('sumar/<int:n1>/<int:n2>', views.sumar),
    path('restar/<int:n1>/<int:n2>', views.restar),
    path('multiplicar/<int:n1>/<int:n2>', views.multiplicar),
    path('dividir/<int:n1>/<int:n2>', views.dividir)


]
