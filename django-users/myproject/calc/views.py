from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse

def sumar(request, n1, n2):
    return HttpResponse(n1 + n2)

def restar(request, n1, n2):
    return HttpResponse(n1-n2)

def multiplicar(request, n1, n2):
    return HttpResponse(n1 * n2)

def dividir(request, n1, n2):
    return HttpResponse(n1/n2)